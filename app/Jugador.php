<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    protected $filable = [
        'nombre',
        'estatura',
        'peso'
    ];

}
