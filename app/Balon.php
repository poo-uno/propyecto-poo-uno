<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balon extends Model
{
    protected $filable = [
        'diametro',
        'volumen'
    ];
}
