@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR UN PRODUCTOS</div>
                <div class="col text-right">
                    <a href= "{{route('list.productos') }}"class="btn btn-sm btn-Success">Cancelar</a>

                <div class="card-body">

                <form role="form" method="post" action="{{url ('guardar.productos')}}">
                {{ csrf_field() }}
                {{ method_field('post') }}

            
                

                    <div class="row">
                       <div class="col-lg-4"
                       <label class="from-control-label" for="nombre"> Nombre del producto</label>
                       <input type="text" class="from-control" name="nombre">
                </div>

                <div class="col-lg-4"
                       <label class="from-control-label" for="tipo"> Tipo de producto</label>
                       <input type="text" class="from-control" name="tipo">
                </div>

                <div class="col-lg-4"
                       <label class="from-control-label" for="estado"> Estado del producto</label>
                       <input type="text" class="from-control" name="estado">
                </div>

                <div class="col-lg-4"
                       <label class="from-control-label" for="Precio"> Precio del producto</label>
                       <input type="text" class="from-control" name="precio">
                </div>

                </div> 
                
                <button type="submit" class="btn btb-success pull-right"> guardar </button>

                
                </form>
                    
  

                </div>
            </div>
        </div>
    </div>
</div>

@endsection